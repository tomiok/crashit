package com.poochie.utilities;

import com.poochie.domain.Address;
import com.poochie.domain.City;
import com.poochie.domain.Country;
import com.poochie.domain.CountryCode;
import com.poochie.domain.Phone;
import com.poochie.domain.Province;
import com.poochie.domain.User;

public class TestUtils {

  private TestUtils() {
  }

  public static User createUser(final String[] name) {
    final User user = new User();

    final Phone phone = new Phone();
    phone.setCountryCode(CountryCode.AR);
    phone.setNumber("3415491538");

    final Address address = new Address();
    final City rosario = new City();
    final Province santaFe = new Province();
    final Country country = new Country();

    country.setId(1L);
    country.setCode("AR");
    country.setName("Argentina");

    santaFe.setId(1L);
    santaFe.setCountry(country);
    santaFe.setName("Santa Fe");

    rosario.setId(1L);
    rosario.setName("Rosario");
    rosario.setPostalCode("2000");
    rosario.setProvince(santaFe);

    address.setCity(rosario);
    address.setStreet("street");
    address.setNumber("123");

    if (name[0] != null) {
      user.setFirstName(name[0]);
    } else {
      user.setFirstName("tomas");
    }
    user.setLastName("lingotti");
    user.setPassword("pass1234");
    user.setEmail("tomas@msn.com");
    user.setDni("00100100");
    user.setPhone(phone);
    user.setAddress(address);

    return user;
  }
}
