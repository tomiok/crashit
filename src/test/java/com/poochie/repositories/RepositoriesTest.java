package com.poochie.repositories;

import com.poochie.CrashitApp;
import com.poochie.domain.User;
import com.poochie.utilities.TestUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
@SpringBootTest
@ContextConfiguration(classes = {CrashitApp.class})
@Ignore
public class RepositoriesTest {

  @Autowired
  private TestEntityManager entityManager;
  @Autowired
  private UserRepository userRepository;
  private User user;

  @Before
  public void setUp() {
    final String[] names = {"Tomi"};
    user = TestUtils.createUser(names);

    entityManager.persist(user.getAddress().getCity().getProvince().getCountry());
    entityManager.persist(user.getAddress().getCity().getProvince());
    entityManager.persist(user.getAddress().getCity());
    entityManager.persist(user.getAddress());
    entityManager.persist(user);
  }

  @Test
  @Transactional
  public void shouldFindUserByDni() {
    final String dni = "00100100";
    final String name = "tomas";
    final String city = "Rosario";

    final Optional<User> userOptional = userRepository.getWithDNI(dni);

    Assert.assertTrue(userOptional.isPresent());
    final User u = userOptional.get();
    assertThat(u.getDni(), is(dni));
    assertThat(u.getFirstName(), is(name));
    assertThat(u.getAddress().getCity().getName(), is(city));
  }

  @Test
  @Transactional
  public void shouldFindListOfUsersByName() {
    int listSize = 1;
    final String name = "tomas";
    final List<User> users = userRepository.getUsersByName(name);
    assertFalse(users.isEmpty());
    assertThat(users.size(), is(listSize));
  }

  @Test
  @Transactional
  public void shouldReturnEmptyListWithUnknownName() {
    final String unknownName = "unknown";
    final List<User> users = userRepository.getUsersByName(unknownName);

    assertTrue(users.isEmpty());
  }
}
