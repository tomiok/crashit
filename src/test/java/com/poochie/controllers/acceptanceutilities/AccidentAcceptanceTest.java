package com.poochie.controllers.acceptanceutilities;

import com.poochie.controllers.utilities.acceptance.AcceptanceUtils;
import com.poochie.domain.AccidentAcceptance;
import org.junit.Test;

import java.util.Optional;
import java.util.Random;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by tomas.lingotti on 30/08/17.
 */
public class AccidentAcceptanceTest {

  @Test
  public void shouldReturn_1_whenNumOfAcceptedIsZero() {
    final AccidentAcceptance accidentAcceptance = createAcceptance("34732706", "00010001", false, 0,
            null);
    final AccidentAcceptance acceptanceUpdated = AcceptanceUtils
            .resolve(Optional.of(accidentAcceptance), "34732706");

    assertThat(acceptanceUpdated.getId(), is(accidentAcceptance.getId()));
    assertThat(acceptanceUpdated.getNumOfAcceptance(), is(1));
    assertThat(acceptanceUpdated.getAcceptedBy(), is("34732706"));
    assertFalse(acceptanceUpdated.getBothAccepted());
  }

  private AccidentAcceptance createAcceptance(String firstDni, String secondDni,
                                              boolean bothAccepted, int numOfAccepted, String acceptedBy) {
    return new AccidentAcceptance(firstDni, secondDni, bothAccepted, numOfAccepted, acceptedBy);
  }

  @Test
  public void shouldReturn_2_whenNumOfAcceptedIsOne() {
    final AccidentAcceptance accidentAcceptance = createAcceptance("34732706", "00010001", false, 1,
            "34732706");
    assertThat(accidentAcceptance.getAcceptedBy(), is("34732706"));

    final AccidentAcceptance acceptanceUpdated = AcceptanceUtils
            .resolve(Optional.of(accidentAcceptance), "00010001");

    assertThat(acceptanceUpdated.getId(), is(accidentAcceptance.getId()));
    assertThat(acceptanceUpdated.getAcceptedBy(), is("00010001"));
    assertThat(acceptanceUpdated.getNumOfAcceptance(), is(2));
    assertTrue(acceptanceUpdated.getBothAccepted());
  }

  @Test
  public void shouldReturnNullWhenIsAlreadyAcceptedByBoth() {
    final AccidentAcceptance accidentAcceptance = createAcceptance("34732706", "00010001", true, 2,
            "34732706");
    final AccidentAcceptance acceptanceUpdated = AcceptanceUtils
            .resolve(Optional.of(accidentAcceptance), "00010001");

    assertNull(acceptanceUpdated);
  }

  @Test(expected = Exception.class)
  public void shouldReturnExceptionWhenIsAlreadyAcceptedByBoth() {
    final AccidentAcceptance accidentAcceptance = createAcceptance("34732706", "00010001", true, 1,
            "34732706");
    AcceptanceUtils.resolve(Optional.of(accidentAcceptance), "00010001");

    fail();
  }

  @Test(expected = Exception.class)
  public void shouldReturnExceptionWhenBothAcceptedIsFalseAndNumOfAcceptedIsGreaterOrEquals_2() {
    final Random r = new Random();
    int accepted = r.ints(2, 4).findAny().getAsInt();

    final AccidentAcceptance accidentAcceptance =
            createAcceptance("34732706", "00010001", false, accepted, "34732706");
    AcceptanceUtils.resolve(Optional.of(accidentAcceptance), "00010001");

    fail();
  }

  @Test(expected = RuntimeException.class)
  public void shouldReturnExceptionWhenAccidentAcceptanceIsNull() {
    final AccidentAcceptance fakeAccAcceptance = null;
    AcceptanceUtils.resolve(Optional.ofNullable(fakeAccAcceptance), "34732706");
  }
}
