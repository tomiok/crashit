package com.poochie.controllers.utilities;

import com.poochie.controllers.jwtutils.JwtClaimer;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

/**
 * Created by tomas.lingotti on 21/08/17.
 */
public class TokenExtractorTest {

  private static final String TOKEN = "Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIzNDczMjcwNyIsImV4cCI6MTUxMTkwMzcxMn0.PE6SBjS-KmbHRTVuWWGKbczkBokM0wuK4KXeQKlLMVw";

  @Test
  public void shouldReturnOnlyTheToken() {
    final String knownToken = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIzNDczMjcwNyIsImV4cCI6MTUxMTkwMzcxMn0.PE6SBjS-KmbHRTVuWWGKbczkBokM0wuK4KXeQKlLMVw";
    final String bearer = "Bearer";
    final String onlyToken = JwtClaimer.extractToken(TOKEN);
    assertFalse(onlyToken.contains(bearer));
    assertThat(onlyToken, is(knownToken));
  }

  @Test
  public void shouldReturnValidDni() {
    final String knownDni = "34732707";
    final String dni = JwtClaimer.claims(TOKEN);
    assertThat(dni, is(knownDni));
  }
}
