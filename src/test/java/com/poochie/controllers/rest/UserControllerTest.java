package com.poochie.controllers.rest;

import com.google.gson.Gson;
import com.poochie.controllers.UserController;
import com.poochie.domain.User;
import com.poochie.repositories.UserRepository;
import com.poochie.services.UserDataCreationService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static com.poochie.utilities.TestUtils.createUser;
import static java.util.Collections.singletonList;
import static org.hamcrest.core.Is.is;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by tomas.lingotti on 01/09/17.
 */
@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
@TestPropertySource(properties = "security.basic.enabled=false")
public class UserControllerTest extends BaseController {

  private static final String DNI = "34732706";
  private static final String NAME = "someName";

  @MockBean
  private UserRepository userRepository;
  @MockBean
  private UserDataCreationService userDataCreationService;
  @MockBean
  private BCryptPasswordEncoder encoder;
  private User userRequest;
  private Gson gson;

  @Before
  public void setUp() {
    final String[] names = {"Tomas"};
    userRequest = createUser(names);
    gson = new Gson();
  }

  @Test
  public void shouldReturnCreatedUser() throws Exception {
    when(encoder.encode(anyString())).thenReturn("passencoded");
    when(userRepository.save(any(User.class))).thenReturn(userRequest);
    doNothing().when(userDataCreationService).saveUserData(userRequest);

    mockMvc
            .perform(post("/user")
                    .content(gson.toJson(userRequest))
                    .contentType(APPLICATION_JSON))
            .andExpect(status().is(HttpStatus.CREATED.value()))
            .andExpect(jsonPath("$.userId").isString())
            .andExpect(jsonPath("$.firstName", is("Tomas")))
            .andExpect(jsonPath("$.lastName", is("lingotti")))
            .andExpect(jsonPath("$.userType").doesNotExist())
            .andExpect(header().stringValues("Content-type", "text/plain;charset=UTF-8"));
  }

  @Test
  public void shouldReturnSelectedUserWhenNameIsProvided() throws Exception {
    when(userRepository.getUsersByName(anyString())).thenReturn(singletonList(userRequest));

    mockMvc
            .perform(get("/user/search/name?name={name}", userRequest.getFirstName())
                    .contentType(APPLICATION_JSON))
            .andExpect(jsonPath("$.[0].firstName", is("Tomas")))
            .andExpect(jsonPath("$.[0].lastName", is("lingotti")))
            .andExpect(status().is(HttpStatus.OK.value()));
  }

  @Test
  public void shouldReturnExceptionWhenOptionalIsNotPresent() throws Exception {
    final User fake = null;
    when(userRepository.getWithDNI(anyString())).thenReturn(Optional.ofNullable(fake));

    mockMvc
            .perform(get("/user/search?dni={num}", DNI)
                    .contentType(APPLICATION_JSON).content(gson.toJson(fake)))
            .andExpect(status().is(HttpStatus.NOT_FOUND.value()))
            .andExpect(jsonPath("$.httpStatus", is("NOT_FOUND")))
            .andExpect(jsonPath("$.message", is("User not found with that DNI.")));
  }

  @Test
  public void shouldReturn404WhenBadRequestIsProvided() throws Exception {
    when(userRepository.getUsersByName(anyString()))
            .thenReturn(singletonList(userRequest));

    mockMvc
            .perform(get("/user/search/name?dni={name}", "name"))
            .andExpect(status().is(HttpStatus.BAD_REQUEST.value()));
  }
}
