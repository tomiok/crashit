package com.poochie.controllers.rest;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;

public abstract class BaseController {

  @Autowired
  protected MockMvc mockMvc;

  private Gson gsonInstance;

  protected Gson getGsonInstance() {
    return new Gson();
  }
}
