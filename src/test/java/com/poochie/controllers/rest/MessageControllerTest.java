package com.poochie.controllers.rest;

import com.poochie.controllers.MessageController;
import com.poochie.domain.User;
import com.poochie.repositories.MessageRepository;
import com.poochie.repositories.TempAccidentRepository;
import com.poochie.repositories.UserRepository;
import com.poochie.services.RetroFeedbackService;
import com.poochie.utilities.TestUtils;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Optional;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(MessageController.class)
@TestPropertySource(properties = "security.basic.enabled=false")
public class MessageControllerTest extends BaseController {

  @MockBean
  private MessageRepository messageRepository;
  @MockBean
  private UserRepository userRepository;
  @MockBean
  private RetroFeedbackService retroFeedbackService;
  @MockBean
  private TempAccidentRepository tempAccidentRepository;

  private String token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIzNDczMjcwNyIsImV4cCI6MTUxMTkwMzcxMn0.PE6SBjS-KmbHRTVuWWGKbczkBokM0wuK4KXeQKlLMVw";

  @Test
  public void shouldReturn_NotPendingMessage_WhenMsgRepositoryReturnsEmpty() throws Exception {
    Mockito.when(messageRepository.getMessageToMe(anyString())).thenReturn(Optional.empty());
    final MvcResult response = mockMvc.perform(get("/messages/get-messages")
            .secure(true)
            .header("Authorization", "Bearer ".concat(token))
            .content(MediaType.APPLICATION_JSON.toString()))
            .andExpect(status().is(HttpStatus.OK.value()))
            .andReturn();

    final String result = response.getResponse().getContentAsString();
    Assert.assertThat(result, Is.is("[ \"No messages in pending status.\" ]"));
  }


  @Test
  public void shouldReturnExceptionWith_ZeroRowsUpdated() throws Exception {
    Mockito.when(messageRepository.acceptMessage(anyString())).thenReturn(0);
    mockMvc
            .perform(post("/messages/accept")
                    .header("Authorization", "Bearer ".concat(token)))
            .andReturn();
  }

  @Test
  public void testAccept() throws Exception {
    final String[] names = {"Tomas"};
    final User senderUser = TestUtils.createUser(names);
    final User receiverUser = TestUtils.createUser(names);
    Mockito.when(messageRepository.acceptMessage(anyString())).thenReturn(1);
    Mockito.when(messageRepository.retroFeedback(anyString())).thenReturn(Optional.of(senderUser));
    Mockito.when(userRepository.getWithDNI(anyString())).thenReturn(Optional.of(receiverUser));
    Mockito.when(retroFeedbackService.retro_sendData(anyObject(), anyString()))
            .thenReturn(senderUser.getDni());
    mockMvc.perform(post("/messages/accept")
            .header("Authorization", "Bearer ".concat(token)))
            .andExpect(jsonPath("$[0].method", Is.is("POST")));
  }
}
