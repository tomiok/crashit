package com.poochie.controllers.rest;

import com.poochie.controllers.LoginController;
import com.poochie.domain.UserLoginRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(LoginController.class)
@TestPropertySource(properties = "security.basic.enabled=false")
public class LoginControllerTest extends BaseController {

  @Test
  public void shouldReturnOKLoginAndHeader() throws Exception {
    final UserLoginRequest ulr = new UserLoginRequest();
    ulr.setDni("34732706");
    ulr.setPassword("secret");
    mockMvc
            .perform(post("/login")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(getGsonInstance().toJson(ulr)))
            .andExpect(status().is(HttpStatus.OK.value()));
  }
}
