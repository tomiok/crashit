package com.poochie;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by tomas.lingotti on 14/08/17.
 */
@SpringBootApplication
public class CrashitApp {

  public static void main(String[] args) {
    SpringApplication.run(CrashitApp.class, args);
  }
}
