package com.poochie.domain;

/**
 * Created by tomas.lingotti on 20/08/17.
 */
public enum MessageStatus {

  ACCEPTED, PENDING, REJECTED;
}
