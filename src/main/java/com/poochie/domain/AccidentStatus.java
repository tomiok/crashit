package com.poochie.domain;

/**
 * Created by tomas.lingotti on 27/08/17.
 */
public enum AccidentStatus {
  NOT_STARTED, IN_PROGRESS, FINALIZED
}
