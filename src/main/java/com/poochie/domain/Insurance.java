package com.poochie.domain;

import com.poochie.config.PersistentObject;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

/**
 * Created by tomas.lingotti on 15/08/17.
 */
@Entity
@Setter
@Getter
public class Insurance extends PersistentObject {

  private String insuranceNumber;
  private String insuranceCompany;
  private String domainPhotoLocation;


}
