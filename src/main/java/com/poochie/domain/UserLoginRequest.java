package com.poochie.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by tomas.lingotti on 20/08/17.
 */
@Getter
@Setter
public class UserLoginRequest {

  private String dni;
  private String password;
}
