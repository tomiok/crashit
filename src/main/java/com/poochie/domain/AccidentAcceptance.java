package com.poochie.domain;

import com.poochie.config.PersistentObject;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

/**
 * Created by tomas.lingotti on 28/08/17.
 */
@Entity
@Getter
@Setter
@AllArgsConstructor
public class AccidentAcceptance extends PersistentObject {

  private String firstDni;
  private String secondDni;
  private Boolean bothAccepted;
  private Integer numOfAcceptance;
  private String acceptedBy;

  private AccidentAcceptance() {
    /**
     *
     * required by JPA.
     */
  }
}
