package com.poochie.domain;

import com.poochie.config.PersistentObject;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Entity
@Getter
@Setter
public class Comment extends PersistentObject {

  private String userDni;
  private String comment;
}
