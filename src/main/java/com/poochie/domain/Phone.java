package com.poochie.domain;

import com.poochie.config.PersistentObject;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

/**
 * Created by tomas.lingotti on 15/08/17.
 */
@Entity
@Getter
@Setter
public class Phone extends PersistentObject {

  private CountryCode countryCode;
  private String number;
}
