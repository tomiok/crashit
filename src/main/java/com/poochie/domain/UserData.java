package com.poochie.domain;

import com.poochie.config.PersistentObject;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

/**
 * Created by tomas.lingotti on 28/08/17.
 */
@Entity
@Getter
@Setter
@AllArgsConstructor
public class UserData extends PersistentObject {

  private String firstName;
  private String lastName;
  private String email;
  private String dni;
  @OneToOne
  private Phone phone;
  @OneToOne
  private Address address;

  public UserData(final User user) {
    this(user.getFirstName(), user.getLastName(), user.getEmail(), user.getDni(), user.getPhone(),
            user.getAddress());
  }

  private UserData() {
    /**
     * required by JPA.
     */
  }
}
