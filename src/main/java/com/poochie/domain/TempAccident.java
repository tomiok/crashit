package com.poochie.domain;

import com.poochie.config.PersistentObject;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import java.time.Instant;

@Entity
@Getter
@Setter
@AllArgsConstructor
public class TempAccident extends PersistentObject {

  @ManyToOne
  private User firstUser;
  @ManyToOne
  private User secondUser;
  private Instant created;
  @Enumerated(EnumType.STRING)
  private AccidentStatus status;
  private Float latitude;
  private Float longitude;

  private TempAccident() {
    // Required by JPA.
  }
}
