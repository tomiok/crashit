package com.poochie.domain;

import com.poochie.config.PersistentObject;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

/**
 * Spanish: grua.
 */
@Entity
@Getter
@Setter
@EqualsAndHashCode(of = {"dni", "domain", "insurance"}, callSuper = false)
@AllArgsConstructor
public class Tow extends PersistentObject {

  private String firstName;
  private String lastName;
  private String dni;
  private String password;
  private String domain;
  @OneToOne(cascade = CascadeType.ALL)
  private Phone phone;
  private String insurance;

  private Tow() {
    /**
     * required by JPA.
     */
  }
}
