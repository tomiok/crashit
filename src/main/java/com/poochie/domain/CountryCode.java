package com.poochie.domain;

/**
 * Created by tomas.lingotti on 15/08/17.
 */
public enum CountryCode {
  AR, BR
}
