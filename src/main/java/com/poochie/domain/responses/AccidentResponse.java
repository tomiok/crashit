package com.poochie.domain.responses;

import com.poochie.domain.Accident;
import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.ResourceSupport;

/**
 * Created by tomas.lingotti on 25/08/17.
 */
@Getter
@Setter
public class AccidentResponse extends ResourceSupport {

  //  private final String photoLocation1;
//  private final String photoLocation2;
//  private final String photoLocation3;
  private final UserResponse firstUser;
  private final UserResponse secondUser;

  public AccidentResponse(final Accident accident) {
    //this.photoLocation1 = accident.getPhotoLocation1();
    //this.photoLocation2 = accident.getPhotoLocation2();
    //this.photoLocation3 = accident.getPhotoLocation3();
    this.firstUser = new UserResponse(accident.getFirstUser());
    this.secondUser = new UserResponse(accident.getSecondUser());
  }
}
