package com.poochie.domain.responses;

import com.poochie.domain.City;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by tomas.lingotti on 22/08/17.
 */
@Getter
@Setter
public class LiteCity {

  private long id;
  private String name;
  private LiteProvince province;

  public LiteCity(final City city) {
    this.id = city.getId();
    this.name = city.getName();
    this.province = new LiteProvince(city.getProvince());
  }
}
