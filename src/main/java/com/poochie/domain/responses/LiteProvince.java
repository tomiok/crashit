package com.poochie.domain.responses;

import com.poochie.domain.Province;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by tomas.lingotti on 22/08/17.
 */
@Getter
@Setter
public class LiteProvince {

  private long id;
  private String name;

  public LiteProvince(final Province province) {
    this.id = province.getId();
    this.name = province.getName();
  }
}
