package com.poochie.domain.responses;

import com.poochie.domain.Country;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * Created by tomas.lingotti on 22/08/17.
 */
@Getter
@Setter
public class DemographicResponse implements Serializable {

  private static final long serialVersionUID = -82394823942319023L;

  private List<Country> countries;
  private List<LiteProvince> provinces;
  private List<LiteCity> cities;
}
