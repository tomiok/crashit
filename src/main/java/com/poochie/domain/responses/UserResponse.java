package com.poochie.domain.responses;

import com.poochie.domain.Address;
import com.poochie.domain.Phone;
import com.poochie.domain.User;
import com.poochie.domain.UserType;
import com.poochie.domain.Vehicle;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.hateoas.ResourceSupport;

/**
 * Created by tomas.lingotti on 20/08/17.
 */
@Getter
@AllArgsConstructor
public class UserResponse extends ResourceSupport {

  private String userId;
  private String firstName;
  private String lastName;
  private String dni;
  private Address address;
  private Phone phone;
  private String email;
  private String photoLocation;
  private Vehicle vehicle;
  private UserType userType;

  public UserResponse(final User user) {
    this(user.getId(), user.getFirstName(), user.getLastName(), user.getDni(), user.getAddress(),
            user.getPhone(), user.getEmail(), user.getPhotoLocation(),
            user.getVehicle(), user.getUserType());
  }

}
