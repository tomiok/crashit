package com.poochie.domain.responses;

import com.poochie.domain.Phone;
import com.poochie.domain.Tow;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class TowHttpResponse {

  private String firstName;
  private String lastName;
  private String dni;
  private String domain;
  private Phone phone;
  private String insurance;

  public TowHttpResponse(final Tow tow) {
    this(tow.getFirstName(), tow.getLastName(), tow.getDni(), tow.getDomain(), tow.getPhone(), tow.getInsurance());
  }
}
