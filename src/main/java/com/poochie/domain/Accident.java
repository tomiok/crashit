package com.poochie.domain;

import com.poochie.config.PersistentObject;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.time.Instant;
import java.util.List;

/**
 * Created by tomas.lingotti on 25/08/17.
 */
@Entity
@Getter
@Setter
public class Accident extends PersistentObject {

  @ManyToOne
  private User firstUser;
  @ManyToOne
  private User secondUser;
  @Column(name = "photo_location_1")
  @Lob
  private byte[] photoLocation1;
  @Column(name = "photo_location_2")
  @Lob
  private byte[] photoLocation2;
  @Column(name = "photo_location_3")
  @Lob
  private byte[] photoLocation3;
  @Enumerated(EnumType.STRING)
  private AccidentType accidentType;
  private Instant created;
  @Enumerated(EnumType.STRING)
  private AccidentStatus status;
  private Float latitude;
  private Float longitude;
  @OneToMany
  private List<Comment> comment;
  private String referenceId;
}
