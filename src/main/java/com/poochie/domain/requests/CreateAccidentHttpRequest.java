package com.poochie.domain.requests;

import com.poochie.domain.Accident;
import com.poochie.domain.AccidentType;

public class CreateAccidentHttpRequest implements ConvertibleToDomain<Accident> {

  private String firstDni;
  private String longDni;
  private AccidentType accidentType;

  @Override
  public Accident toDomain() {
    return null;
  }
}
