package com.poochie.domain.requests;

import com.poochie.domain.Phone;
import com.poochie.domain.Tow;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CreateTowHttpRequest implements ConvertibleToDomain<Tow> {

  @NotNull(message = "The first name cannot be null.")
  private String firstName;
  private String lastName;
  @NotNull(message = "The DNI cannot be null.")
  private String dni;
  private String password;
  @NotNull(message = "The domain cannot be null.")
  private String domain;
  private Phone phone;
  private String insurance;

  @Override
  public Tow toDomain() {
    return new Tow(firstName, lastName, dni, password, domain, phone, insurance);
  }
}
