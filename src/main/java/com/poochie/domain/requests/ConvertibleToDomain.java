package com.poochie.domain.requests;

public interface ConvertibleToDomain<T> {
  T toDomain();
}
