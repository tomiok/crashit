package com.poochie.domain;

import com.poochie.config.PersistentObject;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

/**
 * Created by tomas.lingotti on 14/08/17.
 */
@Entity
@Getter
@Setter
public class Address extends PersistentObject {

  private String street;
  private String number;
  @OneToOne
  private City city;
}
