package com.poochie.domain;

import com.poochie.config.PersistentObject;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

/**
 * Created by tomas.lingotti on 15/08/17.
 */
@Entity
@Getter
@Setter
public class Vehicle extends PersistentObject {

  private String trend;
  private String modelYear;
  private String domain;

}
