package com.poochie.domain;

import com.poochie.config.PersistentObject;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by tomas.lingotti on 20/08/17.
 */
@Entity
@Getter
@Setter
@AllArgsConstructor
public class Message extends PersistentObject {

  @ManyToOne(optional = false, targetEntity = User.class)
  @JoinColumn(name = "from_user")
  private User fromUserDNI;

  @ManyToOne(optional = false, targetEntity = User.class)
  @JoinColumn(name = "to_user")
  private User toUserDNI;

  @Enumerated(EnumType.STRING)
  private MessageStatus status;

  private Message() {
    //Required for JPA.
  }
}
