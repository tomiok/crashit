package com.poochie.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by tomas.lingotti on 14/08/17.
 */
@Entity
@Table(name = "provinces")
@Getter
@Setter
public class Province implements Serializable {

  private static final long serialVersionUID = 1L;
  @Id
  private Long id;
  private String name;
  @OneToOne
  private Country country;
}
