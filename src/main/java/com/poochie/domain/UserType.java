package com.poochie.domain;

/**
 * Created by tomas.lingotti on 25/08/17.
 */
public enum UserType {

  HOLDER, ASSOCIATE
}
