package com.poochie.domain;

import com.poochie.config.PersistentObject;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

/**
 * Created by tomas.lingotti on 23/08/17.
 */
@Entity
@Getter
@Setter
@AllArgsConstructor
public class UserBucket extends PersistentObject {

  public boolean issueFinished;
  @OneToOne
  public User user;

  private UserBucket() {
    /**
     * required for JPA.
     */
  }
}
