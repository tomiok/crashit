package com.poochie.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.poochie.config.PersistentObject;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;

/**
 * Created by tomas.lingotti on 14/08/17.
 */
@Entity
@Getter
@Setter
public class User extends PersistentObject {

  private String firstName;
  private String lastName;
  @Column(unique = true)
  private String dni;
  @OneToOne(cascade = CascadeType.ALL)
  private Address address;
  @OneToOne(cascade = CascadeType.ALL)
  private Phone phone;
  @Column(unique = true)
  private String email;
  private String photoLocation;
  @OneToOne(cascade = CascadeType.ALL)
  private Vehicle vehicle;
  private String password;
  @Enumerated(EnumType.STRING)
  private UserType userType;
  @OneToOne(cascade = CascadeType.ALL)
  @JsonIgnore
  private UserBucket userBucket;
}
