package com.poochie.controllers;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;

public abstract class BaseControllerSerializer {

  @Autowired
  protected Gson gson;

  protected PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();

  JsonElement toJsonElement(final HttpMethod method, final String... args) {
    final JsonArray jsonArray = new JsonArray();
    final JsonObject response = new JsonObject();
    final String uri = "/crashit/accident?first=" + args[0] +
            "&second=" +
            args[1] +
            "&uri=" +
            args[2] +
            "&type=" +
            args[3];

    response.addProperty("uri", uri);
    response.addProperty("method", method.toString());

    jsonArray.add(response);
    return jsonArray;
  }

  <T> String toJsonString(final T object) {
    final JsonElement jsonElement = convertToJsonElement(object);
    return gson.toJson(jsonElement);
  }

  private <T> JsonElement convertToJsonElement(T object) {
    return gson.toJsonTree(object, new TypeToken<T>() {
    }.getType());
  }
}
