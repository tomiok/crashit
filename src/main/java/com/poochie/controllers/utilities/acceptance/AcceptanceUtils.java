package com.poochie.controllers.utilities.acceptance;

import com.poochie.domain.AccidentAcceptance;
import com.poochie.errorhandling.EntityNotFoundException;

import java.util.Optional;

/**
 * Created by tomas.lingotti on 30/08/17.
 */
public class AcceptanceUtils {

  private AcceptanceUtils() {
    throw new RuntimeException("Never instantiate this class.");
  }

  public static AccidentAcceptance resolve(final Optional<AccidentAcceptance> aaOptional,
                                           final String currentDni) {

    if (aaOptional.isPresent()) {
      final AccidentAcceptance aa = aaOptional.get();

      if (check(aa.getBothAccepted(), aa.getNumOfAcceptance(), aa.getAcceptedBy(), currentDni)) {
        return null;
      } else {
        aa.setAcceptedBy(currentDni);

        int numOfAcceptance = aa.getNumOfAcceptance();

        aa.setNumOfAcceptance(numOfAcceptance + 1);

        if (numOfAcceptance == 1) {
          aa.setBothAccepted(true);
        }

        return aa;
      }
    }
    throw new EntityNotFoundException("Not accident acceptance created fot those IDs.");
  }

  private static boolean check(boolean bothAccepted, int numOfAccepted, String acceptedBy,
                               String currentDni) {
    if (!bothAccepted && numOfAccepted >= 2) {
      throw new RuntimeException();
    }

    if (bothAccepted && numOfAccepted != 2) {
      throw new RuntimeException();
    }

    return acceptedBy != null && acceptedBy.equals(currentDni) || bothAccepted;

  }
}
