package com.poochie.controllers.utilities.accident;

import com.poochie.domain.Accident;
import com.poochie.domain.AccidentType;
import com.poochie.domain.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class AccidentUtils {

  private static final Logger logger = LoggerFactory.getLogger(AccidentUtils.class);
  private static final String MESSAGE = "The file was not uploaded";

  public static Accident createAccident(final List<User> users,
                                        final AccidentType accidentType,
                                        final Float latitude,
                                        final Float longitude,
                                        final List<MultipartFile> files) {
    final Accident accident = new Accident();
    accident.setFirstUser(users.get(0));
    accident.setSecondUser(users.get(1));
    accident.setAccidentType(accidentType);
    accident.setLatitude(latitude);
    accident.setLongitude(longitude);
    checkFiles(files);

    Optional.ofNullable(files.get(0)).ifPresent(f -> {
      try {
        accident.setPhotoLocation1(f.getBytes());
      } catch (final IOException e) {
        logger.warn(MESSAGE, e);
      }
    });

    Optional.ofNullable(files.get(1)).ifPresent(f -> {
      try {
        accident.setPhotoLocation2(f.getBytes());
      } catch (final IOException e) {
        logger.warn(MESSAGE, e);
      }
    });

    Optional.ofNullable(files.get(2)).ifPresent(f -> {
      try {
        accident.setPhotoLocation3(f.getBytes());
      } catch (final IOException e) {
        logger.warn(MESSAGE, e);
      }
    });

    return accident;
  }


  private static void checkFiles(final List<MultipartFile> files) {
    for (final MultipartFile file : files) {
      boolean accept = false;
      if (file != null) {
        accept = true;
      }
      if (!accept) {
        throw new RuntimeException();
      }
    }
  }
}
