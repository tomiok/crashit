package com.poochie.controllers;

import com.poochie.domain.UserLoginRequest;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by tomas.lingotti on 20/08/17.
 */
@RestController
@RequestMapping(path = "/login")
public class LoginController {

  /**
   * This method should be empty since the logic to validate was delegated to {@class
   * com.poochie.filters.JWTAuthenticationFilter} and {@class com.poochie.filters.JWTAuthorizationFilter}
   *
   * @param request the request that have DNI and password to validate.
   */
  @PostMapping
  @ApiOperation(value =
          "Send the login request, the response will be ok-200, but won't have content since this endpoint"
                  +
                  "is managed by the JWT filter." +
                  "The Bearer Authorization token will be in the header response.")
  public void login(@RequestBody final UserLoginRequest request) throws Exception {
  }
}
