package com.poochie.controllers;

import com.google.gson.JsonElement;
import com.poochie.controllers.jwtutils.JwtClaimer;
import com.poochie.domain.AccidentStatus;
import com.poochie.domain.AccidentType;
import com.poochie.domain.Message;
import com.poochie.domain.MessageStatus;
import com.poochie.domain.TempAccident;
import com.poochie.domain.User;
import com.poochie.errorhandling.EntityNotFoundException;
import com.poochie.errorhandling.MessageNotUpdatedException;
import com.poochie.repositories.MessageRepository;
import com.poochie.repositories.TempAccidentRepository;
import com.poochie.repositories.UserRepository;
import com.poochie.services.RetroFeedbackService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.ResponseEntity.ok;

/**
 * Created by tomas.lingotti on 20/08/17.
 */
@RestController
@RequestMapping(path = "/messages")
public class MessageController extends BaseControllerSerializer {

  private final MessageRepository messageRepository;
  private final UserRepository userRepository;
  private final RetroFeedbackService retroFeedbackService;
  private final TempAccidentRepository tempAccidentRepository;

  @Autowired
  public MessageController(final MessageRepository messageRepository,
                           final UserRepository userRepository,
                           final RetroFeedbackService retroFeedbackService,
                           final TempAccidentRepository tempAccidentRepository) {
    this.messageRepository = messageRepository;
    this.userRepository = userRepository;
    this.retroFeedbackService = retroFeedbackService;
    this.tempAccidentRepository = tempAccidentRepository;
  }

  /**
   * @return The String with the status ok, or else, the http error.
   */
  @PostMapping
  @ApiOperation(value = "This method need the Bearer token, got in <b>/login</b> Resource." +
          "The Token need to be included as the normal Authorization header." +
          "In the request param 'to', we need to put the DNI for the user that will receive the message."
          +
          "This Resource is secured with the gateway.")
  public ResponseEntity<String> sendMessage(final HttpServletRequest request,
                                            @RequestParam("to") final String dniTo,
                                            @RequestParam("lat") final float lat,
                                            @RequestParam("longitude") final float longitude) {

    final String header = request.getHeader(AUTHORIZATION);
    final String currentDni = JwtClaimer.claims(header);

    final Optional<User> userFromOptional = userRepository.getWithDNI(currentDni);
    final Optional<User> userToOptional = userRepository.getWithDNI(dniTo);

    if (userFromOptional.isPresent() && userToOptional.isPresent()) {
      final Message message = new Message(userFromOptional.get(), userToOptional.get(),
              MessageStatus.PENDING);
      messageRepository.save(message);

      final TempAccident tempAccident =
              new TempAccident(userFromOptional.get(), userToOptional.get(), Instant.now(),
                      AccidentStatus.NOT_STARTED,
                      lat, longitude);

      final TempAccident tempSaved = tempAccidentRepository.save(tempAccident);
      final String temporalAccidentId = tempSaved.getId();
      return ok(gson.toJson(temporalAccidentId));
    }
    throw new EntityNotFoundException("Some of the users are not in the database");
  }

  /**
   * This endpoint accept the message to create the accident, and send the inherit data to each
   * other.
   *
   * @param request the request to identify de user with the token.
   * @return a JSON structure containing the method to create de accident between to people.
   */
  @Transactional
  @PostMapping(path = "/accept")
  @ApiOperation(value =
          "Accept the message and send the data to the sender, this endpoint returns the link" +
                  "to create the accident.")
  public ResponseEntity<String> acceptPendingMessage(final HttpServletRequest request)
          throws Exception {
    final String header = request.getHeader(AUTHORIZATION);
    final String currentDni = JwtClaimer.claims(header);
    int numOfRowsUpdated = messageRepository.acceptMessage(currentDni);

    if (numOfRowsUpdated <= 0) {
      throw new MessageNotUpdatedException("no rows updated, something wrong");
    }

    final String secondDni = retroFeedbackService.retro_sendData(messageRepository, currentDni);

        /*final Link link = linkTo(methodOn(AccidentController.class)
                .createAccident(currentDni, secondDni,"{strUri}", AccidentType.STREET_ACCIDENT))
                .withRel(POST.toString());
        */
    final JsonElement response = toJsonElement(
            HttpMethod.POST,
            currentDni,
            secondDni,
            "[strUri]",
            AccidentType.STREET_ACCIDENT.toString());

    return ok(toJsonString(response));
  }

  /**
   * View the pending messages.
   */
  @GetMapping(path = "/get-messages")
  @ApiOperation(value = "List all the pending messages (if any) to the person looking in the app.")
  public ResponseEntity<List<String>> viewPendingMessage(HttpServletRequest request) {
    final String header = request.getHeader(AUTHORIZATION);
    final String currentDni = JwtClaimer.claims(header);
    final Optional<List<Message>> listOptional = messageRepository.getMessageToMe(currentDni);
    if (listOptional.isPresent()) {
      List<Message> messages = listOptional.get();
      return ok(messages.stream()
              .map(Message::getFromUserDNI)
              .map(User::getDni)
              .collect(toList()));
    }
    return ok(singletonList("No messages in pending status."));
  }
}
