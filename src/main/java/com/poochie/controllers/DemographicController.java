package com.poochie.controllers;

import com.poochie.domain.City;
import com.poochie.domain.responses.DemographicResponse;
import com.poochie.services.DemographicService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.http.ResponseEntity.ok;

/**
 * Created by tomas.lingotti on 22/08/17.
 */
@RestController
@RequestMapping(path = "/demographics")
public class DemographicController {

  private final DemographicService demographicService;

  @Autowired
  public DemographicController(DemographicService demographicService) {
    this.demographicService = demographicService;
  }

  @GetMapping
  @ApiOperation(value =
          "**** A TESTING ENDPOINT **** This resource is NOT in a nutshell, is to obtain the list of all the countries, provinces and cities"
                  +
                  "which are saved in the database. This is a very heavy response :( .")
  public ResponseEntity<DemographicResponse> getDemographics() {
    return ok(demographicService.getDemographicResponse());
  }

  @GetMapping(path = "/search")
  @ApiOperation(value = "This resource is not in a nutshell, is to obtain the province and country for a city.")
  public ResponseEntity<List<City>> getDemographicsByCity(@RequestParam("city") final String city) {
    return ok(demographicService.getAccurateCityResponse(city));
  }
}
