package com.poochie.controllers;

import com.poochie.controllers.jwtutils.JwtClaimer;
import com.poochie.controllers.utilities.acceptance.AcceptanceUtils;
import com.poochie.domain.AccidentAcceptance;
import com.poochie.repositories.AccidentAcceptanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.Optional;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.ResponseEntity.created;

/**
 * Created by tomas.lingotti on 30/08/17.
 */
@RestController
@RequestMapping(path = "/acceptance")
public class AccidentAcceptanceController {

  private final AccidentAcceptanceRepository accidentAcceptanceRepository;

  @Autowired
  public AccidentAcceptanceController(
          final AccidentAcceptanceRepository accidentAcceptanceRepository) {
    this.accidentAcceptanceRepository = accidentAcceptanceRepository;
  }

  @Transactional
  @PutMapping
  public ResponseEntity<String> acceptance(@PathVariable("fDni") final String fDni,
                                           @PathVariable("sDni") final String sDni,
                                           final HttpServletRequest request) {

    final String header = request.getHeader(AUTHORIZATION);
    final String currentDni = JwtClaimer.claims(header);

    final Optional<AccidentAcceptance> aaOptional = accidentAcceptanceRepository
            .getByBothDni(fDni, sDni);

    final AccidentAcceptance aa = AcceptanceUtils.resolve(aaOptional, currentDni);
    if (aa != null) {
      accidentAcceptanceRepository.save(aa);
      return ResponseEntity.ok("Acceptance edited");
    }

    return ResponseEntity.ok("The acceptance was hitted by both or by the same id twice.");
  }

  /**
   * This resource create an accident acceptance. This object models a way to accept both people
   * involved to accept that the accident is in progress, stucked, finished, etc. To move on, both
   * people involved need to "hit" the acceptance.
   *
   * @param fDni the first dni involved.
   * @param sDni the second dni involved.
   * @return the body created.
   */
  @PostMapping
  public ResponseEntity<AccidentAcceptance> createAcceptance(
          @PathVariable("fDni") final String fDni,
          @PathVariable("sDni") final String sDni) throws Exception {

    final AccidentAcceptance aa = new AccidentAcceptance(fDni, sDni, false, 0, null);
    final AccidentAcceptance response = accidentAcceptanceRepository.save(aa);

    return created(new URI(String.format("acceptance?fDni=%s&sDni=%s", fDni, sDni)))
            .body(response);
  }
}
