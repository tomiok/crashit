package com.poochie.controllers;

import com.poochie.domain.User;
import com.poochie.domain.responses.UserResponse;
import com.poochie.errorhandling.EntityNotFoundException;
import com.poochie.repositories.UserRepository;
import com.poochie.services.UserDataCreationService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

import static com.poochie.controllers.utilities.queries.QueryUtils.toLike;
import static java.util.stream.Collectors.toList;
import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping(value = "/user")
public class UserController extends BaseControllerSerializer {

  private final UserRepository userRepository;
  private final BCryptPasswordEncoder encoder;
  private final UserDataCreationService userDataCreationService;

  @Autowired
  public UserController(final UserRepository userRepository,
                        final BCryptPasswordEncoder encoder,
                        final UserDataCreationService userDataCreationService) {
    this.userRepository = userRepository;
    this.encoder = encoder;
    this.userDataCreationService = userDataCreationService;
  }

  @PostMapping
  @ApiOperation(value = "Create a new User, this resource is not in a nutshell.")
  public ResponseEntity<String> createUser(@RequestBody @NotNull final User user) throws Exception {
    user.setPassword(encoder.encode(user.getPassword()));
    final User userSaved = userRepository.save(user);
    final UserResponse userResponse = new UserResponse(userSaved);
    //save the user data isolated for further research in other apps.
    userDataCreationService.saveUserData(userSaved);
    return new ResponseEntity<>(toJsonString(userResponse), HttpStatus.CREATED);
  }

  @GetMapping(path = "/search")
  @ApiOperation(value = "Search the user by ID, this resource is not in a nutshell.")
  public ResponseEntity<String> getUserByDni(@RequestParam("dni") final String dni) {
    final User user = userRepository
            .getWithDNI(dni)
            .orElseThrow(() -> new EntityNotFoundException("User not found with that DNI."));

    return ok(toJsonString(new UserResponse(user)));
  }

  @GetMapping(path = "/search/name")
  public ResponseEntity<List<UserResponse>> getUserByName(@RequestParam("name") final String name) {
    return ok(userRepository
            .getUsersByName(toLike(name))
            .stream()
            .map(UserResponse::new)
            .collect(toList()));
  }

  @GetMapping(path = "/search/domain")
  public ResponseEntity<String> getUserByVehicleDomain(@RequestParam("domain") final String domain) {
    final Optional<User> userOptional = userRepository.searchUserByDomain(toLike(domain));
    if (!userOptional.isPresent()) {
      throw new EntityNotFoundException("No vehicles found with that domain.");
    }
    return ok(toJsonString(userOptional.get()));
  }
}
