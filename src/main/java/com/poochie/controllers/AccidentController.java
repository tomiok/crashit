package com.poochie.controllers;

import com.poochie.controllers.utilities.accident.AccidentUtils;
import com.poochie.domain.Accident;
import com.poochie.domain.AccidentType;
import com.poochie.domain.TempAccident;
import com.poochie.domain.User;
import com.poochie.domain.responses.AccidentResponse;
import com.poochie.repositories.AccidentRepository;
import com.poochie.repositories.TempAccidentRepository;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.ResponseEntity.ok;

/**
 * Created by tomas.lingotti on 25/08/17.
 */
@RestController
@RequestMapping(path = "/accident")
public class AccidentController {

  private final AccidentRepository accidentRepository;
  private final TempAccidentRepository tempAccidentRepository;

  @Autowired
  public AccidentController(final AccidentRepository accidentRepository,
                            final TempAccidentRepository tempAccidentRepository) {
    this.accidentRepository = accidentRepository;
    this.tempAccidentRepository = tempAccidentRepository;
  }

  @PostMapping(path = "/{temp}/{accident}")
  @ApiOperation(value =
          "Create a new accident :( this endpoint is in a nutshell. But no data is grabbed from" +
                  " the headers since we have all the things necessary in the URI.")
  public ResponseEntity<AccidentResponse> createAccident(@PathVariable("temp") final String tempId,
                                                         @PathVariable("accident") final AccidentType accidentType,
                                                         @RequestParam("photo1") final MultipartFile file,
                                                         @RequestParam("photo1") final MultipartFile file2,
                                                         @RequestParam("photo1") final MultipartFile file3) throws Exception {

    final TempAccident temp = tempAccidentRepository.findOne(tempId);
    final List<User> users = Stream.of(temp.getFirstUser(), temp.getSecondUser()).collect(toList());
    if (users.size() != 2) {
      throw new RuntimeException("Cannot get other number than 2 people involved in the accident");
    }
    final List<MultipartFile> files = Stream.of(file, file2, file3).collect(toList());
    final Accident accident = AccidentUtils.createAccident(users, accidentType,
            temp.getLatitude(), temp.getLongitude(), files);
    final Accident accidentSaved = accidentRepository.save(accident);
    final AccidentResponse response = new AccidentResponse(accidentSaved);

    response.add(linkTo(methodOn(AccidentAcceptanceController.class)
            .createAcceptance(users.get(0).getDni(), users.get(1).getDni())).withRel(POST.toString()));
    return new ResponseEntity<>(response, CREATED);
  }

  /**
   * This endpoint is to check in real time the number of accidents.
   *
   * @return {@link AccidentResponse}
   */
  @GetMapping
  public ResponseEntity<List<AccidentResponse>> viewAllAccidents() {
    return ok(accidentRepository
            .findAll()
            .stream()
            .map(AccidentResponse::new)
            .collect(toList()));
  }

  @GetMapping(path = "/{dni}")
  public ResponseEntity<List<AccidentResponse>> getAccidentById(
          @PathVariable("dni") final String dni) {
    return ok(accidentRepository
            .findAllByUserDni(dni)
            .stream()
            .map(AccidentResponse::new)
            .collect(toList()));
  }

  @GetMapping(path = "/get-types")
  @ApiOperation(value = "See all the accident types.")
  public ResponseEntity<Collection<AccidentType>> getAccidentTypes() {
    return ok(EnumSet.allOf(AccidentType.class));
  }
}