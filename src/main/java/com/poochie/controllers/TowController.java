package com.poochie.controllers;

import com.google.i18n.phonenumbers.NumberParseException;
import com.poochie.domain.Phone;
import com.poochie.domain.Tow;
import com.poochie.domain.requests.CreateTowHttpRequest;
import com.poochie.domain.responses.TowHttpResponse;
import com.poochie.errorhandling.EntityNotFoundException;
import com.poochie.repositories.TowRepository;
import com.poochie.validator.ApiValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/tow")
public class TowController extends BaseControllerSerializer {

  private final ApiValidator validator;
  private final TowRepository towRepository;
  private final BCryptPasswordEncoder encoder;

  @Autowired
  public TowController(final ApiValidator validator, final TowRepository towRepository,
                       final BCryptPasswordEncoder encoder) {
    this.validator = validator;
    this.towRepository = towRepository;
    this.encoder = encoder;
  }

  @PostMapping
  public ResponseEntity<String> createTow(@RequestBody final CreateTowHttpRequest request) throws Exception {
    validate(request);
    request.setPassword(encoder.encode(request.getPassword()));
    final Tow tow = towRepository.save(request.toDomain());
    return new ResponseEntity<>(toJsonString(new TowHttpResponse(tow)), HttpStatus.CREATED);
  }

  private void validate(final CreateTowHttpRequest request) throws NumberParseException {
    validator.validate(request);
    final Phone phone = extractPhone(request);
    phoneUtil.isValidNumber(phoneUtil.parse(phone.getNumber(), phone.getCountryCode().toString()));
  }

  private Phone extractPhone(final CreateTowHttpRequest request) {
    return request.getPhone();
  }

  @GetMapping(path = "/{id}")
  public ResponseEntity<String> getTowById(@PathVariable("id") final String id) {
    final Tow tow = towRepository.findOne(id);
    return ok(toJsonString(new TowHttpResponse(tow)));
  }

  @GetMapping(path = "/search")
  public ResponseEntity<String> getTowByDomain(@RequestParam("domain") final String domain) {
    final Tow tow = towRepository.findByDomain(domain)
            .orElseThrow(() -> new EntityNotFoundException("Not tows founded with that domain."));

    return ok(toJsonString(new TowHttpResponse(tow)));
  }
}
