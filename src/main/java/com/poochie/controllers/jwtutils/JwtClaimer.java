package com.poochie.controllers.jwtutils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

import static com.poochie.filters.SecurityConstants.BEARER;
import static com.poochie.filters.SecurityConstants.SECRET_KEY;
import static org.springframework.util.StringUtils.startsWithIgnoreCase;

/**
 * Created by tomas.lingotti on 21/08/17.
 */
public class JwtClaimer {

  public static String claims(final String bearerToken) {
    final String token = extractToken(bearerToken);

    Claims claims = Jwts
            .parser()
            .setSigningKey(SECRET_KEY)
            .parseClaimsJws(token)
            .getBody();

    return claims.getSubject();
  }

  public static String extractToken(final String bearerToken) {
    if (startsWithIgnoreCase(bearerToken, BEARER)) {
      return bearerToken.replaceFirst(BEARER, "");
    }
    throw new RuntimeException("Not safe to decrypt the token.");
  }
}
