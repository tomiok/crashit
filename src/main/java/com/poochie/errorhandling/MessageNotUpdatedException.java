package com.poochie.errorhandling;

import lombok.Getter;

@Getter
public class MessageNotUpdatedException extends RuntimeException {

  private final String message;

  public MessageNotUpdatedException(final String message) {
    this.message = message;
  }
}
