package com.poochie.errorhandling;

import lombok.Getter;

@Getter
public class EntityNotFoundException extends RuntimeException {

  private final String message;

  public EntityNotFoundException(final String message) {
    this.message = message;
  }
}
