package com.poochie.errorhandling;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class ApiExceptionHandler {

  @ExceptionHandler(value = {EntityNotFoundException.class})
  @ResponseBody
  public ResponseEntity<HttpError> customAdvice_EntityNotFound(final EntityNotFoundException e) {
    return create(e, HttpStatus.NOT_FOUND);
  }

  private static ResponseEntity<HttpError> create(final RuntimeException e,
                                                  final HttpStatus status) {
    return createHttpResponse(e, status);
  }

  private static ResponseEntity<HttpError> createHttpResponse(final RuntimeException e,
                                                              final HttpStatus status) {
    final String message = e.getMessage();
    final HttpHeaders headers = new HttpHeaders();
    headers.add("content-type", "application/json");
    final HttpError apiError = HttpError.create(e.getMessage(), status);
    return new ResponseEntity<>(apiError, headers, apiError.getHttpStatus());
  }

  @ExceptionHandler(value = {MessageNotUpdatedException.class})
  @ResponseBody
  public ResponseEntity<HttpError> customAdvice_MessageNotUpdated(final MessageNotUpdatedException e) {
    return create(e, HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler(value = {NullPointerException.class})
  @ResponseBody
  public ResponseEntity<HttpError> advice_NullPointerException(final NullPointerException e) {
    return create(e, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(value = {IllegalArgumentException.class})
  @ResponseBody
  public ResponseEntity<HttpError> advice_IllegalArgument(final IllegalArgumentException e) {
    return create(e, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(value = {RuntimeException.class})
  @ResponseBody
  public ResponseEntity<HttpError> defaultAdvice(final RuntimeException e) {
    return create(e, HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
