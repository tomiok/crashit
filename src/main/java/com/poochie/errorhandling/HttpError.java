package com.poochie.errorhandling;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class HttpError {

  private HttpStatus httpStatus;
  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  private String message;

  public static HttpError create(final String message, final HttpStatus status) {
    return createWithMessage(status, message);
  }

  private static HttpError createWithMessage(final HttpStatus status, final String message) {
    return new HttpError(status, message);
  }

  public static HttpError create(final HttpStatus status) {
    return createWithMessage(status, null);
  }
}
