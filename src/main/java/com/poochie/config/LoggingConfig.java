package com.poochie.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.filter.CommonsRequestLoggingFilter;

import java.util.Arrays;

@Configuration
public class LoggingConfig {

  @Autowired
  private Environment environment;

  @Bean(destroyMethod = "destroy")
  public CommonsRequestLoggingFilter logFilter() {
    final CommonsRequestLoggingFilter filter = new CommonsRequestLoggingFilter();
    if (isNotProd(environment.getActiveProfiles(), "prod")) {
      filter.setIncludeHeaders(true);
    }
    filter.setIncludeQueryString(true);
    filter.setIncludeClientInfo(true);
    filter.setIncludePayload(true);
    filter.setMaxPayloadLength(5012);
    filter.setAfterMessagePrefix("Request: ");
    return filter;
  }

  private static boolean isNotProd(final String activeProfiles[], final String... targetProfiles) {
    return Arrays.stream(activeProfiles).noneMatch(actualProf -> check(actualProf, targetProfiles));
  }

  private static boolean check(final String actualProfile, final String... targetProfiles) {
    return Arrays.asList(targetProfiles).contains(actualProfile);
  }

}
