package com.poochie.config;

import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.UUID;

/**
 * Created by tomas.lingotti on 14/08/17.
 */
@MappedSuperclass
@EqualsAndHashCode
public class PersistentObject implements Identifiable<String>, Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @Column(columnDefinition = "VARCHAR(64)")
  private String id = this.generate();

  @Override
  public String getId() {
    return id;
  }

  private String generate() {
    return UUID.randomUUID().toString();
  }
}
