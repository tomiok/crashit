package com.poochie.config;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationStrategy;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;

/**
 * Created by tomas.lingotti on 14/08/17.
 */
@Configuration
@Profile({"local"})
public class BeansConfiguration {

  @Bean
  public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
    return args -> {

      System.out.println("Let's inspect the beans provided by Spring Boot:");

      //String[] beanNames = ctx.getBeanDefinitionNames();
     // Arrays.sort(beanNames);
     // for (String beanName : beanNames) {
        //  System.out.println(beanName);
    ///  }
    };
  }

  @Bean
  public BCryptPasswordEncoder bCryptPasswordEncoder() {
    return new BCryptPasswordEncoder();
  }

  @Bean
  public CorsConfigurationSource config() {
    final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    source
            .registerCorsConfiguration("/crashit/**", new CorsConfiguration().applyPermitDefaultValues());
    return source;
  }

  @Bean
  public FlywayMigrationStrategy cleanMigrationStrategy() {
    return flyway -> {
      flyway.clean();
      flyway.migrate();
    };
  }

}

