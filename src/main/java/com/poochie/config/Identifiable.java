package com.poochie.config;

/**
 * Created by tomas.lingotti on 14/08/17.
 */
public interface Identifiable<T> {

  T getId();
}
