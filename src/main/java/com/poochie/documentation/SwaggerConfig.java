package com.poochie.documentation;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

/**
 * Created by tomaslingotti on 7/19/17.
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

  @Bean
  public Docket productApi() {
    return new Docket(DocumentationType.SWAGGER_2)
            .select()
            .apis(RequestHandlerSelectors
                    .basePackage("com.poochie"))
            .build()
            .apiInfo(metaData());
  }

  private static ApiInfo metaData() {
    return new ApiInfo("Crashit - App para choques",
            "Facilitar el intercambio de información en los siniestros.",
            "1.0.SNAPSHOT",
            "GNU",
            getMyself(),
            "Apache License V 2.0",
            "https://www.apache.org/licenses/LICENSE-2.0",
            new ArrayList<>());
  }

  private static Contact getMyself() {
    return new Contact("Tomas F. Lingotti", "https:github.com/tomiok", "tomaslingotti@gmail.com");
  }
}
