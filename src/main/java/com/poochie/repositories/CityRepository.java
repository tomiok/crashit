package com.poochie.repositories;

import com.poochie.domain.City;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by tomas.lingotti on 22/08/17.
 */
public interface CityRepository extends JpaRepository<City, Long> {

  List<City> getCityByNameContaining(final String city);
}
