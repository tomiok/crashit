package com.poochie.repositories;

import com.poochie.domain.Country;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by tomas.lingotti on 22/08/17.
 */
public interface CountryRepository extends JpaRepository<Country, Long> {

}
