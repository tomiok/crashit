package com.poochie.repositories;

import com.poochie.domain.User;
import com.poochie.domain.UserBucket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

/**
 * Created by tomas.lingotti on 16/08/17.
 */
public interface UserRepository extends JpaRepository<User, String> {

  @Query(value = "SELECT u FROM User u WHERE u.dni = :dni")
  Optional<User> getWithDNI(@Param("dni") final String dni);

  @Modifying
  @Query(value = "UPDATE User u SET u.userBucket = :ub WHERE u.dni =:dni")
  int updateUserBucket(@Param("ub") final UserBucket userBucket, @Param("dni") final String dni);

  @Query(value = "SELECT u FROM User u WHERE u.dni IN(:fDni, :sDni)")
  List<User> getUsersByDni(@Param("fDni") final String fDni,
                           @Param("sDni") final String sDni);

  @Query(value = "SELECT u FROM User u WHERE u.firstName LIKE :name OR u.lastName LIKE ?1")
  List<User> getUsersByName(final String name);

  @Query(value = "SELECT u FROM User AS u LEFT JOIN u.vehicle AS v WHERE v.domain LIKE ?1")
  Optional<User> searchUserByDomain(final String domain);
}
