package com.poochie.repositories;

import com.poochie.domain.UserBucket;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by tomas.lingotti on 24/08/17.
 */
public interface UserBucketRepository extends JpaRepository<UserBucket, String> {

}
