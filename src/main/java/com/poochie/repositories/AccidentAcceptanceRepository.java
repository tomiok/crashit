package com.poochie.repositories;

import com.poochie.domain.AccidentAcceptance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

/**
 * Created by tomas.lingotti on 28/08/17.
 */
public interface AccidentAcceptanceRepository extends JpaRepository<AccidentAcceptance, String> {

  @Query(value = "SELECT aa FROM AccidentAcceptance aa WHERE (aa.firstDni =:fDni AND aa.secondDni =:sDni)")
  Optional<AccidentAcceptance> getByBothDni(@Param("fDni") final String fDni,
                                            @Param("sDni") final String sDni);
}
