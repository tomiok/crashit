package com.poochie.repositories;

import com.poochie.domain.Tow;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TowRepository extends JpaRepository<Tow, String> {
  Optional<Tow> findByDomain(final String domain);
}
