package com.poochie.repositories;

import com.poochie.domain.UserData;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by tomas.lingotti on 28/08/17.
 */
public interface UserDataRepository extends JpaRepository<UserData, String> {

}
