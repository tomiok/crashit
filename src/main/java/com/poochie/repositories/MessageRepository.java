package com.poochie.repositories;

import com.poochie.domain.Message;
import com.poochie.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

/**
 * Created by tomas.lingotti on 20/08/17.
 */
public interface MessageRepository extends JpaRepository<Message, String> {

  @Query(value = "SELECT m FROM Message m WHERE m.toUserDNI.dni = :toMe")
  Optional<List<Message>> getMessageToMe(@Param("toMe") final String myDni);

  @Modifying
  @Query(value = "UPDATE message m INNER JOIN user u ON u.id = m.to_user SET m.status = 'ACCEPTED' WHERE u.dni = :toMe", nativeQuery = true)
  int acceptMessage(@Param("toMe") final String myDni);

  @Query(value = "SELECT m.fromUserDNI FROM Message m INNER JOIN m.toUserDNI u WHERE u.dni =:toMe")
  Optional<User> retroFeedback(@Param("toMe") final String myDni);
}