package com.poochie.repositories;

import com.poochie.domain.Accident;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by tomas.lingotti on 25/08/17.
 */
public interface AccidentRepository extends JpaRepository<Accident, String> {

  @Query(value = "SELECT a FROM Accident a WHERE a.firstUser IN(:dni) OR a.secondUser IN(:dni)")
  List<Accident> findAllByUserDni(@Param("dni") final String dni);

  @Override
    //@Cacheable("findAll")
  List<Accident> findAll();

  @Override
    // @CacheEvict(value = "findAll", key = "#p0.id")
  Accident save(final Accident entity);

  @Query(value = "UPDATE Accident a SET a.status = :status WHERE a.firstUser.dni =:fDni and a.secondUser.dni = :sDni")
    //@CachePut(cacheNames = "finalAll", key = "#p1")
  int updateCacheStatus(@Param("status") final String status,
                        @Param("fDni") final String fDni,
                        @Param("sDni") final String sDni);


}
