package com.poochie.repositories;

import com.poochie.domain.TempAccident;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TempAccidentRepository extends JpaRepository<TempAccident, String> {

}
