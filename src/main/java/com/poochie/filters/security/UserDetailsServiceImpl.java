package com.poochie.filters.security;

import com.poochie.domain.User;
import com.poochie.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static java.util.Collections.emptyList;

/**
 * Created by tomas.lingotti on 18/08/17.
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

  private final UserRepository userRepository;

  @Autowired
  public UserDetailsServiceImpl(final UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Override
  public UserDetails loadUserByUsername(final String dni) throws UsernameNotFoundException {
    final Optional<User> userOptional = userRepository.getWithDNI(dni);
    if (userOptional.isPresent()) {
      final User user = userOptional.get();
      return new org.springframework.security.core.userdetails.User
              (user.getDni(), user.getPassword(), emptyList());
    }
    throw new UsernameNotFoundException(dni);
  }
}
