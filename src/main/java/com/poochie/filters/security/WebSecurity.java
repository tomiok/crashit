package com.poochie.filters.security;

import com.poochie.filters.JWTAuthenticationFilter;
import com.poochie.filters.JWTAuthorizationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static com.poochie.filters.SecurityConstants.SWAGGER_URLS;
import static com.poochie.filters.SecurityConstants.URL_PATH_ACCEPTED;

/**
 * Created by tomas.lingotti on 18/08/17.
 */
@EnableWebSecurity
public class WebSecurity extends WebSecurityConfigurerAdapter {

  private final UserDetailsService userDetailsService;
  private final BCryptPasswordEncoder bCryptPasswordEncoder;

  @Autowired
  public WebSecurity(final UserDetailsService userDetailsService,
                     final BCryptPasswordEncoder bCryptPasswordEncoder) {
    this.userDetailsService = userDetailsService;
    this.bCryptPasswordEncoder = bCryptPasswordEncoder;
  }

  @Override
  protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
  }

  @Override
  public void configure(
          final org.springframework.security.config.annotation.web.builders.WebSecurity web)
          throws Exception {
    web.ignoring().antMatchers(SWAGGER_URLS);
  }

  @Override
  protected void configure(final HttpSecurity http) throws Exception {
    http.cors().and().csrf().disable().authorizeRequests()
            .antMatchers(HttpMethod.POST, URL_PATH_ACCEPTED)
            .permitAll()
            .antMatchers(HttpMethod.GET, URL_PATH_ACCEPTED)
            .permitAll()
            //  .antMatchers(HttpMethod.POST, URLS_TO_AUTHENTICATE)
            //.authenticated()
            //.antMatchers(HttpMethod.GET, URLS_TO_AUTHENTICATE)
            //.authenticated()
            //.anyRequest().authenticated()
            .and()
            .addFilter(new JWTAuthenticationFilter(authenticationManager()))
            .addFilter(new JWTAuthorizationFilter(authenticationManager()));
  }
}
