package com.poochie.filters;

/**
 * Created by tomas.lingotti on 20/08/17.
 */
public final class SecurityConstants {

  public static final String BEARER = "Bearer ";
  public static final String SECRET_KEY = "supersecretkey";
  public static final String[] URL_PATH_ACCEPTED = {"/login", "/user", "/demographics",
          "/accident/get-types"};
  public static final String[] URLS_TO_AUTHENTICATE = {"/messages/**", "/accident", "/user/search/**"};
  public static final String[] SWAGGER_URLS = {"/v2/api-docs", "/configuration/ui",
          "/swagger-resources", "/configuration/security", "/swagger-ui.html", "/webjars/**"};
  static final String AUTHORIZATION = "Authorization";
  static final String BEARER_TRIMED = "Bearer";
  static final long TOKEN_VALID_DAYS = 99L;

  /**
   * Never instantiate this class.
   */
  private SecurityConstants() {
    throw new RuntimeException("Not instance allowed.");
  }

}
