package com.poochie.filters;

import io.jsonwebtoken.Jwts;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.poochie.filters.SecurityConstants.AUTHORIZATION;
import static com.poochie.filters.SecurityConstants.BEARER_TRIMED;
import static com.poochie.filters.SecurityConstants.SECRET_KEY;
import static java.util.Collections.emptyList;

/**
 * Created by tomas.lingotti on 18/08/17.
 */
public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

  private static final String EMPTY = "";

  public JWTAuthorizationFilter(AuthenticationManager manager) {
    super(manager);
  }

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                                  FilterChain chain) throws IOException, ServletException {

    final String header = request.getHeader(AUTHORIZATION);
    if (header == null || header.isEmpty() || !header.startsWith(BEARER_TRIMED)) {
      chain.doFilter(request, response);
      return;
    }

    final UsernamePasswordAuthenticationToken authenticationFilter = getAuthentication(request);

    SecurityContextHolder.getContext().setAuthentication(authenticationFilter);
    chain.doFilter(request, response);
  }

  private UsernamePasswordAuthenticationToken getAuthentication(final HttpServletRequest request) {
    final String header = request.getHeader(AUTHORIZATION);
    return createAuthToken(header);
  }

  private static UsernamePasswordAuthenticationToken createAuthToken(final String header) {
    if (header == null) {
      return null;
    }

    final String user = Jwts
            .parser()
            .setSigningKey(SECRET_KEY)
            .parseClaimsJws(header.replace(BEARER_TRIMED, EMPTY))
            .getBody()
            .getSubject();

    return user != null ?
            new UsernamePasswordAuthenticationToken(user, null, emptyList()) :
            null;
  }
}
