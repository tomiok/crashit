package com.poochie.filters;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.poochie.domain.UserLoginRequest;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import static com.poochie.filters.SecurityConstants.AUTHORIZATION;
import static com.poochie.filters.SecurityConstants.BEARER;
import static com.poochie.filters.SecurityConstants.SECRET_KEY;
import static com.poochie.filters.SecurityConstants.TOKEN_VALID_DAYS;
import static java.util.Collections.emptyList;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

  private final AuthenticationManager authenticationManager;

  @Autowired
  public JWTAuthenticationFilter(final AuthenticationManager authenticationManager) {
    this.authenticationManager = authenticationManager;
  }

  @Override
  public Authentication attemptAuthentication(final HttpServletRequest request,
                                              final HttpServletResponse response) throws AuthenticationException {
    try {
      final UserLoginRequest user = new ObjectMapper()
              .readValue(request.getInputStream(), UserLoginRequest.class);
      return authenticationManager.authenticate(
              new UsernamePasswordAuthenticationToken(user.getDni(), user.getPassword(), emptyList()));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  protected void successfulAuthentication(final HttpServletRequest request,
                                          final HttpServletResponse response,
                                          final FilterChain chain,
                                          final Authentication authResult) throws IOException, ServletException {
    final String token = Jwts
            .builder()
            .setSubject(((org.springframework.security.core.userdetails.User) authResult.getPrincipal())
                    .getUsername())
            .setExpiration(
                    Date.from(Instant.now().plus(TOKEN_VALID_DAYS, ChronoUnit.DAYS))) // valid token for 99 days
            .signWith(SignatureAlgorithm.HS256, SECRET_KEY)
            .compact();
    response.addHeader(AUTHORIZATION, BEARER + token);
  }
}
