package com.poochie.validator;

import org.springframework.stereotype.Component;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidationException;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by tomas.lingotti on 30/07/17.
 */
@Component
public class ApiValidator {

  private final Validator validator;

  public ApiValidator() {
    ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
    validator = vf.getValidator();
  }

  public <T> void validate(T beanToValidate) {
    final Set<ConstraintViolation<T>> violations = validator.validate(beanToValidate);
    if (!violations.isEmpty()) {
      final String exceptions = violations
              .stream()
              .map(ConstraintViolation::getMessage)
              .collect(Collectors.joining("|"));

      throw new ValidationException("Those validations are thrown" + exceptions);
    }
  }
}
