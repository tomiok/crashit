package com.poochie.services;

import com.poochie.domain.User;
import com.poochie.domain.UserBucket;
import com.poochie.errorhandling.EntityNotFoundException;
import com.poochie.repositories.MessageRepository;
import com.poochie.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Created by tomas.lingotti on 24/08/17.
 */
@Service
public class RetroFeedbackService {

  private final UserRepository userRepository;
  private final Logger logger = LoggerFactory.getLogger(RetroFeedbackService.class);

  @Autowired
  public RetroFeedbackService(final UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  /**
   * This method get the optionals users (sender end receiver) and if both are present, send the
   * user data to each other to the {@see UserBucket}. So both can get the others information.
   *
   * @return the DNI of the sender, to create the link to the new accident.
   */
  public String retro_sendData(final MessageRepository repository, final String currentDni) {
    final Optional<User> userSenderOptional = repository
            .retroFeedback(currentDni); // the optional is the sender
    final Optional<User> userReceiverOptional = userRepository.getWithDNI(currentDni);
    if (userSenderOptional.isPresent() && userReceiverOptional.isPresent()) {
      final User sender = userSenderOptional.get();
      final User receiver = userReceiverOptional.get();

      final UserBucket userBucket = new UserBucket(false, sender); // sender data
      logger.info("Added {} bucket to sender with DNI: {}",
              userRepository.updateUserBucket(userBucket, currentDni), currentDni); // current

      logger.info("Added {} bucket to sender currentDni: {}",
              userRepository.updateUserBucket(new UserBucket(false, receiver), sender.getDni()));

      return sender.getDni();
    }
    throw new EntityNotFoundException("Cannot do retro feedback without a sender");
  }
}
