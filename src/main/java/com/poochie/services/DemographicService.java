package com.poochie.services;

import com.poochie.domain.City;
import com.poochie.domain.responses.DemographicResponse;
import com.poochie.domain.responses.LiteCity;
import com.poochie.domain.responses.LiteProvince;
import com.poochie.repositories.CityRepository;
import com.poochie.repositories.CountryRepository;
import com.poochie.repositories.ProvinceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * Created by tomas.lingotti on 22/08/17.
 */
@Service
public class DemographicService {

  private final CityRepository cityRepository;
  private final ProvinceRepository provinceRepository;
  private final CountryRepository countryRepository;

  private DemographicResponse demographicResponse;

  @Autowired
  public DemographicService(final CityRepository cityRepository,
                            final ProvinceRepository provinceRepository,
                            final CountryRepository countryRepository) {
    this.cityRepository = cityRepository;
    this.provinceRepository = provinceRepository;
    this.countryRepository = countryRepository;
  }

  public List<City> getAccurateCityResponse(final String name) {
    return this.getCitiesByName(name);
  }

  private List<City> getCitiesByName(final String cityName) {
    return cityRepository.getCityByNameContaining(cityName);
  }

  public DemographicResponse getDemographicResponse() {
    if (demographicResponse == null) {
      demographicResponse = new DemographicResponse();

      if (demographicResponse.getCities() == null || demographicResponse.getCities().isEmpty()) {
        fillCities();
      }
      if (demographicResponse.getProvinces() == null || demographicResponse.getProvinces()
              .isEmpty()) {
        fillProvinces();
      }
      if (demographicResponse.getCountries() == null || demographicResponse.getCountries()
              .isEmpty()) {
        fillCountries();
      }

    }
    return demographicResponse; //yield on methods above.
  }

  private void fillCities() {
    demographicResponse
            .setCities(cityRepository.findAll()
                    .stream()
                    .map(LiteCity::new)
                    .collect(toList()));
  }

  private void fillProvinces() {
    demographicResponse
            .setProvinces(provinceRepository.findAll()
                    .stream()
                    .map(LiteProvince::new)
                    .collect(toList()));
  }

  private void fillCountries() {
    demographicResponse.setCountries(countryRepository.findAll());
  }
}
