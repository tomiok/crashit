package com.poochie.services;

import com.poochie.domain.User;
import com.poochie.domain.UserData;
import com.poochie.repositories.UserDataRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by tomas.lingotti on 28/08/17.
 */
@Service
@NoArgsConstructor
public class UserDataCreationService {

  private UserDataRepository userDataRepository;

  @Autowired
  public UserDataCreationService(final UserDataRepository userDataRepository) {
    this.userDataRepository = userDataRepository;
  }

  @Transactional
  public void saveUserData(final User user) {
    final UserData data = new UserData(user);
    userDataRepository.save(data);
  }
}
