CREATE TABLE tow (
  id         VARCHAR(64) NOT NULL,
  first_name VARCHAR(255),
  last_name  VARCHAR(255),
  dni        VARCHAR(255),
  password   VARCHAR(255),
  domain     VARCHAR(255),
  phone_id   VARCHAR(64),
  insurance  VARCHAR(255),
  PRIMARY KEY (id)
);

ALTER TABLE tow
  ADD CONSTRAINT FKtow12341234 FOREIGN KEY (phone_id) REFERENCES phone (id);