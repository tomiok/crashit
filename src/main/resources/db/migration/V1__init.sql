CREATE TABLE accident (
  id               VARCHAR(64) NOT NULL,
  accident_type    VARCHAR(255),
  created          TINYBLOB,
  latitude         FLOAT,
  longitude        FLOAT,
  photo_location_1 LONGBLOB,
  photo_location_2 LONGBLOB,
  photo_location_3 LONGBLOB,
  reference_id     VARCHAR(255),
  status           VARCHAR(255),
  first_user_id    VARCHAR(64),
  second_user_id   VARCHAR(64),
  PRIMARY KEY (id)
);
CREATE TABLE accident_comment (
  accident_id VARCHAR(64) NOT NULL,
  comment_id  VARCHAR(64) NOT NULL
);
CREATE TABLE accident_acceptance (
  id                VARCHAR(64) NOT NULL,
  accepted_by       VARCHAR(255),
  both_accepted     BIT,
  first_dni         VARCHAR(255),
  num_of_acceptance INTEGER,
  second_dni        VARCHAR(255),
  PRIMARY KEY (id)
);
CREATE TABLE address (
  id      VARCHAR(64) NOT NULL,
  number  VARCHAR(255),
  street  VARCHAR(255),
  city_id BIGINT,
  PRIMARY KEY (id)
);
CREATE TABLE cities (
  id          BIGINT NOT NULL,
  name        VARCHAR(255),
  postal_code VARCHAR(255),
  province_id BIGINT,
  PRIMARY KEY (id)
);
CREATE TABLE comment (
  id       VARCHAR(64) NOT NULL,
  comment  VARCHAR(255),
  user_dni VARCHAR(255),
  PRIMARY KEY (id)
);
CREATE TABLE countries (
  id   BIGINT NOT NULL,
  code VARCHAR(255),
  name VARCHAR(255),
  PRIMARY KEY (id)
);
CREATE TABLE insurance (
  id                    VARCHAR(64) NOT NULL,
  domain_photo_location VARCHAR(255),
  insurance_company     VARCHAR(255),
  insurance_number      VARCHAR(255),
  PRIMARY KEY (id)
);
CREATE TABLE message (
  id        VARCHAR(64) NOT NULL,
  status    VARCHAR(255),
  from_user VARCHAR(64) NOT NULL,
  to_user   VARCHAR(64) NOT NULL,
  PRIMARY KEY (id)
);
CREATE TABLE phone (
  id           VARCHAR(64) NOT NULL,
  country_code INTEGER,
  number       VARCHAR(255),
  PRIMARY KEY (id)
);
CREATE TABLE provinces (
  id         BIGINT NOT NULL,
  name       VARCHAR(255),
  country_id BIGINT,
  PRIMARY KEY (id)
);
CREATE TABLE temp_accident (
  id             VARCHAR(64) NOT NULL,
  created        TINYBLOB,
  latitude       FLOAT,
  longitude      FLOAT,
  status         VARCHAR(255),
  first_user_id  VARCHAR(64),
  second_user_id VARCHAR(64),
  PRIMARY KEY (id)
);
CREATE TABLE user (
  id             VARCHAR(64) NOT NULL,
  dni            VARCHAR(255),
  email          VARCHAR(255),
  first_name     VARCHAR(255),
  last_name      VARCHAR(255),
  password       VARCHAR(255),
  photo_location VARCHAR(255),
  user_type      VARCHAR(255),
  address_id     VARCHAR(64),
  phone_id       VARCHAR(64),
  user_bucket_id VARCHAR(64),
  vehicle_id     VARCHAR(64),
  PRIMARY KEY (id)
);
CREATE TABLE user_bucket (
  id             VARCHAR(64) NOT NULL,
  issue_finished BIT         NOT NULL,
  user_id        VARCHAR(64),
  PRIMARY KEY (id)
);
CREATE TABLE user_data (
  id         VARCHAR(64) NOT NULL,
  dni        VARCHAR(255),
  email      VARCHAR(255),
  first_name VARCHAR(255),
  last_name  VARCHAR(255),
  address_id VARCHAR(64),
  phone_id   VARCHAR(64),
  PRIMARY KEY (id)
);
CREATE TABLE vehicle (
  id         VARCHAR(64) NOT NULL,
  domain     VARCHAR(255),
  model_year VARCHAR(255),
  trend      VARCHAR(255),
  PRIMARY KEY (id)
);
ALTER TABLE accident_comment
  ADD CONSTRAINT UK_ijneb0xjq6s4ioo27pnok458q UNIQUE (comment_id);
ALTER TABLE user
  ADD CONSTRAINT UK_jq0ta6mef3p0o47ysw6sflcdl UNIQUE (dni);
ALTER TABLE user
  ADD CONSTRAINT UK_ob8kqyqqgmefl0aco34akdtpe UNIQUE (email);
ALTER TABLE accident
  ADD CONSTRAINT FK8fn2b44utx2ersqyl477iwh9g FOREIGN KEY (first_user_id) REFERENCES user (id);
ALTER TABLE accident
  ADD CONSTRAINT FKm0xqqvkpvr39087q40xkvxcr5 FOREIGN KEY (second_user_id) REFERENCES user (id);
ALTER TABLE accident_comment
  ADD CONSTRAINT FKorwe2dnsnico18nfmtxafoq75 FOREIGN KEY (comment_id) REFERENCES comment (id);
ALTER TABLE accident_comment
  ADD CONSTRAINT FKbed5e0iscmfbueahqhedrl4q0 FOREIGN KEY (accident_id) REFERENCES accident (id);
ALTER TABLE address
  ADD CONSTRAINT FKa6p4hdfq92oyy92gb5ra8xiw7 FOREIGN KEY (city_id) REFERENCES cities (id);
ALTER TABLE cities
  ADD CONSTRAINT FKcf2ndxcsekl26rrkb9egbhq20 FOREIGN KEY (province_id) REFERENCES provinces (id);
ALTER TABLE message
  ADD CONSTRAINT FKar52ihwqd5x7yr5fy0wyccc41 FOREIGN KEY (from_user) REFERENCES user (id);
ALTER TABLE message
  ADD CONSTRAINT FKqudela62q9jomupq41w1omta4 FOREIGN KEY (to_user) REFERENCES user (id);
ALTER TABLE provinces
  ADD CONSTRAINT FK48p9qkti5auert2gquvn76338 FOREIGN KEY (country_id) REFERENCES countries (id);
ALTER TABLE temp_accident
  ADD CONSTRAINT FK78un4mh9qbmelgj9pbqed6mp0 FOREIGN KEY (first_user_id) REFERENCES user (id);
ALTER TABLE temp_accident
  ADD CONSTRAINT FKf7qmb82omrqdxkiqft4c913un FOREIGN KEY (second_user_id) REFERENCES user (id);
ALTER TABLE user
  ADD CONSTRAINT FKddefmvbrws3hvl5t0hnnsv8ox FOREIGN KEY (address_id) REFERENCES address (id);
ALTER TABLE user
  ADD CONSTRAINT FK5vq5p57v94ao9chhr4g398fe9 FOREIGN KEY (phone_id) REFERENCES phone (id);
ALTER TABLE user
  ADD CONSTRAINT FKjfn8gq07q6bmahqtmtdj8lwo7 FOREIGN KEY (user_bucket_id) REFERENCES user_bucket (id);
ALTER TABLE user
  ADD CONSTRAINT FKft982237l4ve4ikalcgmjx14y FOREIGN KEY (vehicle_id) REFERENCES vehicle (id);
ALTER TABLE user_bucket
  ADD CONSTRAINT FKa683hles4n8797hvcfhcj0c9t FOREIGN KEY (user_id) REFERENCES user (id);
ALTER TABLE user_data
  ADD CONSTRAINT FKr07d3wy86wr30y85ksign0j33 FOREIGN KEY (address_id) REFERENCES address (id);
ALTER TABLE user_data
  ADD CONSTRAINT FKa3orosksyi9ivyfl3nmp1l11e FOREIGN KEY (phone_id) REFERENCES phone (id);
